package com.kajeet.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.kajeet.utilities.*;

public class UtilitiesFactory 
{
	static WebDriver driver;
	public static Properties prop = new Properties();
	public static Map<String, String> map = new HashMap<String, String>();

	public static WebDriver startBrowser(String browserName, String url) {
		if (browserName.equals("firefox")) {
			driver = new FirefoxDriver();
		} else if (browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
			driver = new ChromeDriver();
		}
		driver.manage().window().maximize();
		driver.get(url);
		return driver;
	}

	public static WebDriver launchAPP() {
		driver =startBrowser("chrome", "https://www.kajeet.com/sentinel/login.htm");
		return driver;
	}
	public static void scrollPage(int x, int y) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(" + x + "," + y + ")");
	}

	public static void ImplicitWait(int seconds) {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.MILLISECONDS);
	}

	public static void screenShots(String SSfilename) {
		TakesScreenshot ts = (TakesScreenshot) driver;
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		File source = ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source,
					new File(".//ScreenShots" + "_" + SSfilename + "_" + dateFormat.format(date) + ".png"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Screen shot not captured");
		}
		System.out.println("screenshot capture");

	}

	// Reading from File to Map
	public static Map<String, String> getProperty(String source_filename) {

		try {
			FileInputStream fis = new FileInputStream(source_filename);
			prop.load(fis);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Some issue finding or loading file....!!! " + e.getMessage());

		}
		for (final Entry<Object, Object> entry : prop.entrySet()) {
			map.put((String) entry.getKey(), (String) entry.getValue());
		}
		return map;
	}

	// Writing from Map to file
	public static void writeMap(Map Wmap,String output_filename) {
		try {
			File fileOne = new File(output_filename);
			FileOutputStream fos = new FileOutputStream(fileOne);
			SortedProperties sp = new SortedProperties();

			Iterator it = Wmap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				System.out.println(pair.getKey() + " = " + pair.getValue());
				// sp.put(pair.getKey(),pair.getValue());
				if (pair.getValue() != null) {
					sp.setProperty(pair.getKey().toString(), pair.getValue().toString());
				}
			}

			sp.store(fos, null);

		} 
		catch (Exception e) 
		{
			System.out.println(e);
		}
	}

}
