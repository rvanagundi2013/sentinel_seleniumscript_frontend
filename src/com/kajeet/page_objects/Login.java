package com.kajeet.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.kajeet.utilities.UtilitiesFactory;

public class Login 
{
	WebDriver driver;

	// constructor
	public Login(WebDriver ldriver) {
		this.driver = ldriver;

	}

	@FindBy(how = How.CSS,using= "body > app-root > app-user-login > div > div.aw-content > div > div > div > section.col-md.aw-login.aw-section > form > div:nth-child(1) > input")
	@CacheLookup
	WebElement username;

	@FindBy(how = How.CSS, using = "body > app-root > app-user-login > div > div.aw-content > div > div > div > section.col-md.aw-login.aw-section > form > div.form-group.mb-2 > input")
	@CacheLookup
	WebElement password;

	/*@FindBy(how=How.LINK_TEXT, using = "Log in")
	@CacheLookup
	WebElement loginLink;*/
	
	@FindBy(how = How.CSS, using = "body > app-root > app-user-login > div > div.aw-content > div > div > div > section.col-md.aw-login.aw-section > form > button")
	@CacheLookup
	WebElement login_in;

	// login function
	public void application_login(WebDriver driver,String uid, String pwd) 
	{
		
		UtilitiesFactory.ImplicitWait(30);
		username.sendKeys(uid);
		password.sendKeys(pwd);
		login_in.click();

	}

}
