package com.kajeet.testcases;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kajeet.page_objects.*;
import com.kajeet.utilities.UtilitiesFactory;

public class Testcase1 {
	public String Inputfile = ".//dataResources//InputProperties.properties";
	public static Map<String, String> map = new HashMap<String, String>();

	@Test
	public void validateLogin() throws FileNotFoundException, IOException, InterruptedException {
		/*--------------------------------------------------------------------------
		 * DATA load from file to map 
		 *--------------------------------------------------------------------------*/
		map = UtilitiesFactory.getProperty(Inputfile);
		String applicationUname = map.get("USERNAME");
		String applicationPwd = map.get("PASSWORD");

		WebDriver driver = UtilitiesFactory.launchAPP();
		Login login_obj = PageFactory.initElements(driver, Login.class);
		login_obj.application_login(driver,applicationUname, applicationPwd);
		UtilitiesFactory.ImplicitWait(30);
		String currentURL = driver.getCurrentUrl();
		Assert.assertEquals(currentURL, map.get("HOMEPAGEURL"), "Dashboard Page");
		UtilitiesFactory.screenShots("Loginsuccessful");
	}
}
